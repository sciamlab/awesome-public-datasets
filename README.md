# awesome-public-datasets

a list of awesome public datasets released all over the world by public or private organizations

## Coached Conversational Preference Elicitation
URL: https://storage.googleapis.com/dialog-data-corpus/CCPE-M-2019/landing_page.html 
License: CC BY-SA 4.0
Description: https://research.google/tools/datasets/coached-conversational-preference-elicitation/

## Taskmaster-1
URL: https://github.com/google-research-datasets/Taskmaster
License: CC BY-SA 4.0
Description: https://research.google/tools/datasets/taskmaster-1/


## CheXpert
URL: https://stanfordmlgroup.github.io/competitions/chexpert/
Description: 

